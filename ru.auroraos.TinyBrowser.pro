# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TARGET = ru.auroraos.TinyBrowser

CONFIG += \
    c++14 \
    link_pkgconfig \
    auroraapp \
    auroraapp_i18n

TRANSLATIONS += \
    translations/ru.auroraos.TinyBrowser.ts \
    translations/ru.auroraos.TinyBrowser-ru.ts

SOURCES += \
    src/ru.auroraos.TinyBrowser.cpp

DISTFILES += \
    rpm/ru.auroraos.TinyBrowser.spec \
    translations/*.ts \
    ru.auroraos.TinyBrowser.desktop \
    LICENSE.BSD-3-Clause.md \
    CODE_OF_CONDUCT.md \
    CONTRIBUTING.md \
    AUTHORS.md \
    README.md \
    README.ru.md

AURORAAPP_ICONS = 86x86 108x108 128x128 172x172
