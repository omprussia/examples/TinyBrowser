// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.6
import Sailfish.Silica 1.0
import Sailfish.WebView 1.0
import Sailfish.WebEngine 1.0

Page {
    id: root
    objectName: "mainPage"

    property real cutoutOffset: {
        switch (orientation) {
        case Orientation.Portrait:
            return Math.max(SafeZoneRect.insets.top, SafeZoneRect.appInsets.top);
        case Orientation.Landscape:
            return Math.max(SafeZoneRect.insets.left, SafeZoneRect.appInsets.top);
        case Orientation.PortraitInverted:
            return Math.max(SafeZoneRect.insets.bottom, SafeZoneRect.appInsets.top);
        case Orientation.LandscapeInverted:
            return Math.max(SafeZoneRect.insets.right, SafeZoneRect.appInsets.top);
        }
    }

    WebView {
        id: webView
        objectName: "webView"

        property string _initUrl: "http://www.youtube.com"

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }

        height: {
            switch (root.orientation) {
            case Orientation.Portrait:
                {
                    return Screen.height - cutoutOffset - pageFooter.height;
                }
            case Orientation.Landscape:
                {
                    return Screen.width - cutoutOffset - pageFooter.height;
                }
            case Orientation.PortraitInverted:
                {
                    return Screen.height - cutoutOffset - pageFooter.height;
                }
            case Orientation.LandscapeInverted:
                {
                    return Screen.width - cutoutOffset - pageFooter.height;
                }
            }
        }
        url: _initUrl
        footerMargin: cutoutOffset + pageFooter.height
    }

    Rectangle {
        id: pageFooter
        objectName: "pageFooter"

        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        height: Math.max(aboutButton.height, urlField.height) + Theme.paddingMedium * 2
        color: Theme.darkSecondaryColor

        IconButton {
            id: aboutButton

            objectName: "aboutButton"
            anchors {
                left: parent.left
                leftMargin: Theme.paddingMedium
                verticalCenter: parent.verticalCenter
            }
            icon {
                source: "image://theme/icon-m-about"
                sourceSize {
                    width: Theme.iconSizeMedium
                    height: Theme.iconSizeMedium
                }
            }

            onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
        }

        TextField {
            id: urlField

            objectName: "urlField"
            anchors {
                left: aboutButton.right
                right: parent.right
                verticalCenter: parent.verticalCenter
            }
            inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhUrlCharactersOnly
            focusOutBehavior: FocusBehavior.ClearPageFocus
            labelVisible: false
            placeholderText: qsTr("URL")
            textLeftPadding: 0
            textLeftMargin: Theme.paddingMedium
            font {
                pixelSize: Theme.fontSizeLarge
                family: Theme.fontFamilyHeading
            }
            EnterKey.iconSource: "image://theme/icon-m-search"

            EnterKey.onClicked: {
                webView.url = text;
                webView.focus = true;
            }
            Component.onCompleted: urlField.text = webView._initUrl
        }
    }
}
